" --------------------
" Behavior

set autoread

" --------------------
" Colors

colorscheme desert

syntax enable

" ---------------------
" Filetypes

filetype on
filetype plugin on
filetype indent on

" ---------------------
" Spaces and Tabs

set smartindent
set foldenable
set foldmethod=indent
set foldlevel=99

filetype plugin indent on

set tabstop=2 " Number of visual spaces per tab

set shiftwidth=2

set softtabstop=2 " Number of spaces when editing

set expandtab " tabs are spaces

" --------------------
" Ui Config

set relativenumber
set number

set showcmd " show command in the bottom bar

set cursorline " highlight current line TODO: maybe turn this off

set wildmenu " Visual autocomplete for command menu

set lazyredraw " faster drawing

set showmatch " highlight matching brackets

" --------------------
" Searching

set incsearch " search as characters are entered
set hlsearch " highlight matches

" --------------------
" Completion

set infercase
set complete-=i
set complete+=kspell
set completeopt=menuone,noselect,noinsert

" --------------------
" Movement

nnoremap j gj
nnoremap k gk

" --------------------
" Vim-Plug

call plug#begin('~/.config/nvim/plugged')

Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'sebastianmarkow/deoplete-rust', { 'for': 'rust' }
Plug 'rust-lang/rust.vim', { 'for': 'rust' }
Plug 'junegunn/vim-easy-align', { 'on': ['<Plug>(EasyAlign)', 'EasyAlign']}
Plug 'tpope/vim-fugitive'
Plug 'cespare/vim-toml'
Plug 'airblade/vim-gitgutter'
Plug 'ntpeters/vim-better-whitespace'
Plug 'rhysd/committia.vim'
Plug 'sbdchd/neoformat'
Plug 'gisraptor/vim-lilypond-integrator'
Plug 'elixir-lang/vim-elixir'
Plug 'slashmili/alchemist.vim'
Plug 'mustache/vim-mustache-handlebars'
Plug 'chemzqm/vim-jsx-improve'

call plug#end()

" -------------------
" Deoplete

let g:deoplete#enable_at_startup = 1
inoremap <expr><tab> pumvisible() ? "\<c-n>" : "\<tab>"
if !exists('g:deoplete#omni#input_patters')
	let g:deoplete#omni#input_patterns = {}
endif
autocmd InsertLeave,CompleteDone * if pumvisible() == 0 | pclose | endif

" -------------------
" Deoplete rust

let g:deoplete#sources#rust#racer_binary="/home/dnmllr/.cargo/bin/racer"
let g:deoplete#sources#rust#rust_source_path=$RUST_SRC_PATH

" -------------------
" Committia

let g:committia_hooks = {}
function! g:committia_hooks.edit_open(info)
        setlocal spell

        if a:info.vcs ==# 'git' && getline(1) ==# ''
                startinsert
        end

endfunction

" ------------------
" Rust

let g:rustfmt_autosave = 1


" ------------------
" close comments automatically

autocmd FileType c,cpp,java,javascript set formatoptions+=ro
autocmd FileType c set omnifunc=ccomplete#Complete

" ------------------
" Make and asm
autocmd FileType make set noexpandtab shiftwidth=8 softtabstop=8
autocmd FileType asm set noexpandtab shiftwidth=8 softtabstop=8 syntax=nasm
